*** Settings ***
Documentation  API Testing Using Robot Framework
Library  SeleniumLibrary
Library  RequestsLibrary
Library  JSONLibrary
Library  Collections


*** Test Cases ***
TC01 GET Request to validate the response code and response body
    [documentation]  Test case to verify that the response code of the GET Request is 200,
    ...              the response body has a key:value pair as title:London,
    ...              the response body has also a 'location_type' key.
    [tags]  Smoke
    Create Session  metaweathersession  https://www.metaweather.com  verify=true
    ${res}=         GET On Session  metaweathersession  /api/location/search/  params=query=london
    Status Should Be  200  ${res}  # For status 200 check

    # vefify that the title key of the Response Body is London
    ${title}=  Get Value From Json  ${res.json()}[0]  title
    ${titleFromList}=  get from list  ${title}  0
    should be equal  ${titleFromList}  London

    # vefify that the Response body  has a location_type
    ${body}=  convert to string  ${res.content}
    should contain  ${body}  location_type
