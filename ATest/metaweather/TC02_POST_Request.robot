*** Settings ***
Documentation  API Testing Using Robot Framework
Library  SeleniumLibrary
Library  RequestsLibrary
Library  JSONLibrary
Library  Collections


*** Test Cases ***
TC02 POST Request to validate the response code, response body and header
    [documentation]  Test case to verify that the response code of the POST Request is 201,
    ...              the response body has a key:value pair as id:101,
    ...              the Content-Type of the response header is application/json.
    [tags]  Regression
    Create Session  jshsession  https://jsonplaceholder.typicode.com  verify=true
    &{body}=  create dictionary  title=weather  body=chart  userId=9005
    &{header}=  create dictionary  Cache-Control=no-cache
    ${res}=         POST On Session  jshsession  /posts  data=&{body}  headers=&{header}
    Status Should Be  201  ${res}  # For status 201 check

    # vefify that the id of the Response Body is 101
    ${id}=  Get Value From Json  ${res.json()}  id
    ${idFromList}=  get from list  ${id}  0
    ${idFromListAsString}=  convert to string  ${idFromList}
    should be equal as strings  ${idFromListAsString}  101

    # vefify that the Content-Type of the response header is application/json.
    ${headerValue}=  get from dictionary  ${res.headers}  Content-Type
    should be equal  ${headerValue}  application/json; charset=utf-8
