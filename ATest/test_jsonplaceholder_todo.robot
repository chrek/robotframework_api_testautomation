*** Settings ***
Library  SeleniumLibrary
Library  RequestsLibrary
Library  Collections

Documentation   Testing the todo endpoint of REST    https://jsonplaceholder.typicode.com

*** Variables ***
${baseUrl}        https://jsonplaceholder.typicode.com
${endPoint}       /todos/1

*** Test Cases ***
TC_01 GET Request
    create session  todosession    ${baseUrl}
    ${resp}=    get request  todosession    ${endPoint}

#    log to console  ${resp.content}
#    log to console  ${resp.status_code}
#    log to console  ${resp.headers}

    ${code}=    convert to string  ${resp.status_code}

    # verify that response status code is 200
    should be equal  ${code}    200

    ${body}=    convert to string  ${resp.content}

    # verify that response body has "id": 1
    should contain  ${body}    "id": 1

    ${contentTypeValue}=    get from dictionary  ${resp.headers}    Content-Type
    log to console    ${contentTypeValue}

    # verify that Content-Type value equal application/json; charset=utf-8
    should be equal  ${contentTypeValue}    application/json; charset=utf-8
